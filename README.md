# Traversability data set:

Datasets are separated in `contexts`. 

We use our CNN approach for ground traversability estimation (https://arxiv.org/abs/1709.05368) to train a model for our ground robot. Then, we extract the weights from a hidden layer of our CNN to generate sample features. These features are included in the `context` datasets along its respective `true` label.

## Data gathering process

We extract the data from a simulator. A four wheel robot moves around (without purposely steering) a heightmap storing the heightmap patch and its current pose [See attached video]. Heightmaps are artificially generated to include different traversability situations, e.g. plains, slopes, blocks, holes, rough terrain.

With information from the simulator, we extract image patches (from the topview heightmap) in each trajectory followed by the robot. This patches are bigger than the size of the robot to include a view of its surroundings. If the robot manages to traverse such patch in a fixed time, then this patch is labeled as traversable.

## How a context is considered

By modifying a simulation parameter, we change the traversability capabilities of the robot. In this case we change the **m** friction parameter of the robot's wheels. Therefore, each context is defined by this **m** value.

We sample **m** uniformly, from a lower bound (unreliable movement) to the standard friction value (desired friction value), i.e. **m** *=[0.2, 0.9]*.

The following figure is a topview comparison of the trajectories of the robot from the same initial pose, and constant movement command but with a different friction parameter **m**. We can see that the closest **m** is to the desired friction value *0.9* the less variant the trajectories are. 

![topview_trajectories](topview_trajectories.png "topview trajectories for differnt m values")

For this figure, the change interval of **m** is *0.1*. The testing map is a small hill the robot should climb and pass. It only *kind of* succeeds when **m** *>0.4*.

The **m** change interval we used for our context definition was *0.7* for a slightly finer variation.

> Eleven contexts were generated *[0.2, 0.27, 0.34, 0.41, 0.48, 0.55, 0.62, 0.69, 0.76, 0.83, 0.9]*.


## Model training for feature extraction

Using datasets from 5 contexts (*[0.34,0.48,0.55,0.62,0.69]*), we trained a CNN for traversability estimation. For this purpose, this bulk dataset was divided into training and evaluation (%30). The performance of the learned classifier is:

```
ACC: 0.8962
AUC: 0.94390733028
             precision    recall  f1-score   support

        0.0       0.82      0.89      0.85      3426
        1.0       0.94      0.90      0.92      6574

avg / total       0.90      0.90      0.90     10000
```

Confusion matrix:
```
[[3042  384]
 [ 654 5920]]
```

Once the CNN is trained, we passed each of the ten contexts (precisely speaking, a random sampled subset of size *N* per context) over the network and extract the weights of the hidden layer. This layer has `100` weights that will represent the features for the each sample of each context.

> The `100` weights are from a hidden layer with `ReLUs` activation.


This is the rate of true values per context dataset:

```
m_0_2  -> 0.4371
m_0_27 -> 0.5318
m_0_34 -> 0.6003
m_0_41 -> 0.6052
m_0_48 -> 0.5745
m_0_55 -> 0.5874
m_0_62 -> 0.6363
m_0_69 -> 0.6808
m_0_76 -> 0.6997
m_0_83 -> 0.6770
m_0_9  -> 0.6539
```


> NOTE: The CNN classifier was trained using the full datasets from each of the 5 contexts (not only the subsets of size *N*) . Approximately, each full context dataset has 300 000 samples.


## Dataset representation

Each context dataset is stored in a `csv` file with *N* rows. Each row has its ID, followed by 100 feature values (positive real numbers) and ends with the true value of the sample (1 traversable, 0 not).

> Context `csv` files are stored in folder `contexts`. *N=30000*

## Possible changes

- Train the model with a different set of contexts
- Change the number of features

## Notes with respect to the last dataset

- We only focused on one robot
- We changed a simulator value that may render different values of traversability

